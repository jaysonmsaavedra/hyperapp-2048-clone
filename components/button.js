import { h, app } from 'hyperapp'

const Button = (action, content) => {
  <button onclick={(action)}>{content}</button>
}

export default Button