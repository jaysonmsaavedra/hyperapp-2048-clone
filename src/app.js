import { h, app } from 'hyperapp';
import actions from './actions/actions';
import state from './states/state';
import view from './views/view';
import './styles/app.scss'

const main = app(state, actions, view, document.body)
document.body.addEventListener('keypress', main.handleKeypress)