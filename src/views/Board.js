import { h, app } from 'hyperapp'

import Square from './Square'

const Board = ({squares}) => (
  <div>
    {squares.map(square => (
      <Square value={square.value} />
    ))}
  </div>
)

export default Board