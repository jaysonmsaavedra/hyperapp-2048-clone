import { h, app } from 'hyperapp';

const Tile = ({tile}) => {
  return (tile > 0
    ? <div class='square'>{tile}</div>
    : <div class='square empty'></div>
  )
}

const Row = ({row}) => (
  <div class='row'>
    {row.map(tile => (
      <Tile tile={tile} />
    ))}
  </div>
)

const Board = ({tiles}) => (
  <div class='board'>
    {tiles.map(row => <Row row={row} />)}
  </div>
)

const view = (state, actions) => (
  <div>
    <h1>2048</h1>
    <button onclick={() => actions.start()}> Start </button>
    <div class='board'>
      {state.tiles.map(row => <Row row={row} />)}
    </div>
  </div>
)

export default view