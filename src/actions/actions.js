const actions = {
  spawn: () =>  state => {
    let updatedSquares = state.squares

    let square = {
      x: actions.getRandomInt(4),
      y: actions.getRandomInt(4),
      value: actions.getInitialValue()
    }

    updatedSquares.push({x: square.y, y: square.x, value: square.value})

    return square
  },

  updateTiles: squares => (state, actions) => {
    let updatedTiles = state.tiles
    
    squares.forEach(square => {
      updatedTiles[square.x][square.y] = square.value
    })
  },

  addSquare: () => (state, actions) => {
    let square = actions.spawn()

    actions.updateTiles([square])
  },

  start: () => (state, actions) => {
    actions.addSquare()
    actions.addSquare()
  },

  move: direction => (state, actions) => {
    console.log(direction)
    actions.addSquare()
  },

  handleKeypress: e => (state, actions) => {
    switch (e.key) {
      case 'ArrowUp':
        actions.move('up')
        break;
      case 'ArrowDown':
        actions.move('down')
        break;
      case 'ArrowLeft':
        actions.move('left')
        break;
      case 'ArrowRight':
        actions.move('right')
        break;
      default:
        console.log('no good')
        break;
    }
  },

  getRandomInt: max => {
    return Math.floor(Math.random() * Math.floor(max))
  },

  getInitialValue: () => {
    return Math.random() > 0.1 ? 2 : 4
  },
};

export default actions;
